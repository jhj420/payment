module gitee.com/jhj420/payment

go 1.16

replace google.golang.org/grpc v1.45.0 => google.golang.org/grpc v1.26.0

require (
	gitee.com/jhj420/common v0.0.0-20220420123442-5d392a73ac99
	github.com/golang/protobuf v1.5.2
	github.com/jinzhu/gorm v1.9.16
	github.com/micro/go-micro/v2 v2.9.1
	github.com/micro/go-plugins/wrapper/monitoring/prometheus/v2 v2.9.1
	github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2 v2.9.1
	github.com/micro/go-plugins/wrapper/trace/opentracing/v2 v2.9.1
	github.com/opentracing/opentracing-go v1.2.0
	google.golang.org/grpc v1.45.0 // indirect
	google.golang.org/protobuf v1.28.0
)
