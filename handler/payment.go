package handler
import (
	"context"
	"gitee.com/jhj420/common"
	"gitee.com/jhj420/payment/domain/model"
	"gitee.com/jhj420/payment/domain/service"
	payment "gitee.com/jhj420/payment/proto/payment"
)
type Payment struct{
     PaymentDataService service.IPaymentDataService
}

func (p *Payment) AddPayment(ctx context.Context, request *payment.PaymentInfo,response *payment.PaymentID) error {
	payment := &model.Payment{}
	if err := common.SwapTo(request,payment);err != nil{
		common.Error(err)
	}
	paymentID,err := p.PaymentDataService.AddPayment(payment)
	if err != nil{
		common.Error(err)
	}
	response.PaymentId = paymentID
	return nil

}

func (p *Payment) UpdatePayment(ctx context.Context,request *payment.PaymentInfo,response *payment.Response) error {
	payment := &model.Payment{}
	if err := common.SwapTo(request,payment);err != nil{
		common.Error(err)
	}
	if err := p.PaymentDataService.UpdatePayment(payment);err!= nil{
		common.Error(err)
	}
	response.Msg = "更新成功"
	return nil
}

func (p *Payment) DeletePaymentByID(ctx context.Context,request *payment.PaymentID,response *payment.Response) error {
	return p.PaymentDataService.DeletePayment(request.PaymentId)
}

func (p *Payment) FindPaymentByID(ctx context.Context,request *payment.PaymentID,response *payment.PaymentInfo) error {
	payment,err:=p.PaymentDataService.FindPaymentByID(request.PaymentId)
	if err !=nil {
		common.Error(err)
	}
	return common.SwapTo(payment,response)
}

func (p *Payment) FindAllPayment(ctx context.Context,request *payment.All,response *payment.PaymentAll) error {
	allPayment,err:=p.PaymentDataService.FindAllPayment()
	if err !=nil {
		common.Error(err)
	}

	for _,v:=range allPayment{
		paymentInfo := &payment.PaymentInfo{}
		if err:=common.SwapTo(v,paymentInfo);err!=nil{
			common.Error(err)
		}
		response.PaymentInfo = append(response.PaymentInfo,paymentInfo)
	}
	return nil
}



