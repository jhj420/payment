package main

import (
	"gitee.com/jhj420/common"
	"gitee.com/jhj420/payment/domain/repository"
	service2 "gitee.com/jhj420/payment/domain/service"
	"gitee.com/jhj420/payment/handler"
	payment "gitee.com/jhj420/payment/proto/payment"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"github.com/micro/go-plugins/wrapper/monitoring/prometheus/v2"
	ratelimit "github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2"
	opentracing2 "github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"github.com/opentracing/opentracing-go"
)

var(
	QPS = 100
)

func main() {
	//配置中心
	consulConfig, err := common.GetConsulConfig("127.0.0.1", 8500, "/micro/config")
	if err != nil {
		common.Error(err)
	}

	//链路追综
	t, io, err := common.NewTracer("go.micro.service.payment", "127.0.0.1:6831")
	if err != nil {
		common.Error(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)

	//连接数据库
	//获取mysql配置，路径中不带前缀
	mysqlInfo := common.GetMysqlFromConsul(consulConfig, "mysql")
	//连接数据库
	db, err := gorm.Open("mysql", mysqlInfo.User+":"+mysqlInfo.Pwd+"@/"+mysqlInfo.Database+"?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		common.Error(err)
	}
	defer db.Close()
	//禁止复表
	db.SingularTable(true)
	//数据表初始化 不能重复执行
	rp := repository.NewPaymentRepository(db)
	_ = rp.InitTable()

	//添加监控
	//暴露监控地址
	common.PrometheusBoot(9089)


	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.payment"),
		micro.Version("latest"),
		//暴露的服务地址
		micro.Address("127.0.0.1:8085"),
		//注册中心
		//micro.Registry(consul),
		//使用etcd作为注册中心
		micro.Registry(etcd.NewRegistry(
			registry.Addrs("127.0.0.1:2379"))),
		//链路追踪
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		//添加限流
		micro.WrapHandler(ratelimit.NewHandlerWrapper(QPS)),
		//添加监控
		micro.WrapHandler(prometheus.NewHandlerWrapper()),
	)

	// Initialise service
	service.Init()

	paymentDataService := service2.NewPaymentDataService(repository.NewPaymentRepository(db))

	// Register Handler
	err = payment.RegisterPaymentHandler(service.Server(), &handler.Payment{PaymentDataService:paymentDataService})
	if err != nil{
		common.Error(err)
	}
	// Run service
	if err := service.Run(); err != nil {
		common.Fatal(err)
	}
}
